#include "htab.h"

typedef struct node_t {
  char occupied, live;
  void *key, *val;
} node_t;

typedef struct htab_t {
  node_t *table;
  int cap;
  int size;

#if DIAGNOSTICS
  int num_inserts;
  int num_delete_success;
  int num_delete_failure;
  int num_lookup_success;
  int num_lookup_failure;
  int num_expansions;
  int num_collisions;
#endif
} htab_t;

// placeholder hashing function, since for the moment we use only int keys.
static inline int hash(int key) {
  return key;
}

static int __htab_expand(htab_t *htab);


htab_t *htab_create() {
  htab_t *htab = calloc(1, sizeof(htab_t));
  if (!htab) return NULL;

  htab->table = calloc(HTAB_CAP_INIT_DEFAULT, sizeof(node_t));
  if (!htab->table) {
    free(htab);
    return NULL;
  }

  htab->cap  = HTAB_CAP_INIT_DEFAULT;

  // the size field as well as all of the debug fields (num_inserts etc) will
  // have been zero-initialized by calloc().

  return htab;
}

void htab_destroy(htab_t *htab) {
  free(htab->table);
  free(htab);
}


int htab_insert(htab_t *htab, int key, float val) {

  // if table is already saturated, expand it before inserting new item.
  // table is saturated if half or more slots are filled.
  // TODO: inline htab_expand since it is only used here anyway.
  if (htab->size >= htab->cap / 2 &&
      __htab_expand(htab) == FAILURE)
    return FAILURE;

  int k = hash(key) & (htab->cap - 1);

  while (htab->table[k].live) {
    k = (k + 1) & (htab->cap - 1);
#if DIAGNOSTICS
    htab->num_collisions++;
#endif
  }

  htab->table[k].key  = key;
  htab->table[k].val  = val;
  htab->table[k].live = htab->table[k].occupied = 1;

  htab->size++;
#if DIAGNOSTICS
  htab->num_inserts++;
#endif
  return SUCCESS;
}


int htab_lookup(htab_t *htab, int key, float *place) {

  int k = hash(key) % htab->cap;

  node_t node;
  while ((node = htab->table[k]).occupied) {
    if (node.live && node.key == key) {
      *place = node.val;
#if DIAGNOSTICS
      htab->num_lookup_success++;
#endif
      return SUCCESS;
    }
    k = (k + 1) & (htab->cap - 1);
  }

#if DIAGNOSTICS
  htab->num_lookup_failure++;
#endif
  return FAILURE;
}


int htab_delete(htab_t *htab, int key) {

  int k = hash(key) % htab->cap;
  node_t node;
  while ((node = htab->table[k]).occupied) {
    if (node.live && node.key == key) {
      htab->table[k].live = 0;
      htab->size--;
#if DIAGNOSTICS
      htab->num_delete_success++;
#endif
      return SUCCESS;
    }
    k = (k + 1) & (htab->cap - 1);
  }
#if DIAGNOSTICS
  htab->num_delete_failure++;
#endif
  return FAILURE;
}


inline int htab_get_size(htab_t *htab) {
  return htab ? htab->size : -1;
}

inline int htab_get_cap(htab_t *htab) {
  return htab ? htab->cap : -1;
}


void htab_print(htab_t *htab) {
  if (!htab)
    fprintf(stderr, ">> htab NULL!\n");

  else if (htab->size == 0)
    printf("[]\n");

  else {
    printf("[");
    for (int i = 0; i < htab->cap; i++) {
      node_t node = htab->table[i];

      if (node.live)
        printf(" (%d -> %.2f) ", node.key, node.val);

    }
    printf("]\n");
  }
}


/* INTERNALS */
static inline int __htab_expand(htab_t *htab) {

  size_t new_cap    = htab->cap * 2;
  node_t *new_table = calloc(new_cap, sizeof(node_t));
  if (!new_table)
    return FAILURE;

  // copy all live nodes to new table.
  for (int i = 0; i < htab->cap; i++) {
    node_t node = htab->table[i];
    if (node.live) {

      // nodes are rehashed before copying to new table.
      int k = hash(node.key) & (htab->cap - 1);

      // linear probing :)
      while (new_table[k].live)
        k = (k + 1) & (new_cap - 1);

      new_table[k] = node;
    }
  }

  // free old table.
  free(htab->table);

  htab->table = new_table;
  htab->cap = new_cap;
#if DIAGNOSTICS
  htab->num_expansions++;
#endif
  return SUCCESS;
}



/*
 * DIAGNOSTICS STUFF
 */
#if DIAGNOSTICS
void htab_print_diagnostics(htab_t *htab) {
  if (htab)
    printf("/-----------------------------------\\\n"
           "| cap:                   %10d |\n"
           "| size:                  %10d |\n"
           "|                                   |\n"
           "| insertions:            %10d |\n"
           "|                                   |\n"
           "| successful deletions:  %10d |\n"
           "| failed     deletions:  %10d |\n"
           "|                                   |\n"
           "| successful lookups:    %10d |\n"
           "| failed     lookups:    %10d |\n"
           "|                                   |\n"
           "| expansions:            %10d |\n"
           "| collisions:            %10d |\n"
           "\\-----------------------------------/\n",
           htab->cap,                htab->size,
           htab->num_inserts,
           htab->num_delete_success, htab->num_delete_failure,
           htab->num_lookup_success, htab->num_lookup_failure,
           htab->num_expansions,     htab->num_collisions);
}
#endif
