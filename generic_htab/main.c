#include "htab.h"
#include <stdlib.h>
#include <time.h>

#define DIAGNOSTICS 1

int main() {

  srand(time(NULL));

  printf("creating empty htab ...\n");
  htab_t *htab = htab_create();

  const int num_insertions = 8473128;
  const int num_deletions  = 2187472;

  printf("inserting %d random key/value pairs ...\n", num_insertions);
  for (int i = 0; i < num_insertions; i++) {
    int   key = rand() & 0xffffff;
    float val = ((float) rand() / RAND_MAX) * 100.0f;
    htab_insert(htab, key, val);
  }
  htab_print_diagnostics(htab);


  printf("deleting %d random keys ...\n", num_deletions);
  for (int i = 0; i < num_deletions; i++) {
    int key = rand() & 0xffffff;
    htab_delete(htab, key);
  }

  htab_print_diagnostics(htab);

  htab_destroy(htab);
}
