#pragma once

typedef struct node_t {
  int val;
  struct node_t *next;
  struct node_t *prev;
} node_t;

typedef struct list_t {
  struct node_t *head;
  int n;
} list_t;


list_t *new_list() {
  list_t *new = Malloc(sizeof(list_t));
  new->head   = NULL;
  new->n      = 0;
  return new;
}

node_t *new_node(int val) {
  node_t *new = Malloc(sizeof(node_t));
  new->val  = val;
  new->next = new->prev = NULL;
  return new;
}


void insert(list_t *lst, int val) {
  node_t *new = new_node(val);
  new->next = lst->head;
  lst->head = new;
  lst->n++;
}

void delete(list_t *lst, int val) {
  node_t *p = lst->head;
  while (p != NULL) {

    if (p->val == val) {
      p->prev->next = p->next;
      p->next->prev = p->prev;

      free(p);
      lst->n--;
      printf("removed node with val %d!\n", val);
      break;
    }
    p = p->next;
  }
}
