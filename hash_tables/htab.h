#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#define DIAGNOSTICS 1

#define SUCCESS 1
#define FAILURE 0

#define HTAB_CAP_INIT_DEFAULT 2048
#define HTAB_CAP_IS_POW_OF_2  1


typedef struct node_t node_t;
typedef struct htab_t htab_t;



/*
 * htab API
 */
htab_t *htab_create();
void htab_destroy(htab_t *htab);

int htab_insert(htab_t *htab, int key, float val);
int htab_delete(htab_t *htab, int key);
int htab_lookup(htab_t *htab, int key, float *place);

int htab_get_size(htab_t *htab);
int htab_get_cap(htab_t *htab);

void htab_print(htab_t *htab);

#if DIAGNOSTICS
void htab_print_diagnostics(htab_t *htab);
#endif
