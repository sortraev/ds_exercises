#include "htab.h"

typedef struct node_t {
  int   key;
  float val;
  struct node_t *next;
} node_t;

typedef struct htab_t {
  node_t **table;
  int size;
  int cap;
} htab_t;




static inline void __htab_expand(htab_t *htab);

// create a new list node (only used for the linear chaining variant).
node_t *node_create(int key, float val);

void list_destroy(node_t *head);
int list_push(node_t **head, node_t *new);
int list_push_new(node_t **head, int key, float val);
int list_delete(node_t *head, int key);
void list_print(node_t *head);

// technically a hash function on integers.
static inline int hash(int key) {
  return key;
}

inline int htab_get_size(htab_t *htab) {
  return htab->size;
}

inline int htab_get_cap(htab_t *htab) {
  return htab->cap;
}

htab_t *htab_create() {
  htab_t *htab = malloc(sizeof(htab_t));
  if (!htab) return NULL;

  htab->table = calloc(HTAB_CAP_INIT_DEFAULT, sizeof(node_t*));
  if (!htab->table) {
    free(htab);
    return NULL;
  }


  htab->cap   = HTAB_CAP_INIT_DEFAULT;
  htab->size  = 0;
  return htab;
}

void htab_destroy(htab_t *htab) {
  for (int i = 0; i < htab->cap; i++)
    // TODO: should probably inline this since it is not used elsewhere?
    list_destroy(htab->table[i]);
  free(htab->table);
  free(htab);
}




int htab_insert(htab_t *htab, int key, float val) {

  // if inserting new item would saturate the table, expand it first.
  // the table is saturated if half or more slots are filled.
  if (htab->size + 1 > htab->cap / 2)
    __htab_expand(htab);

  int k = hash(key) % htab->cap;

  if (!list_push_new(htab->table + k, key, val))
    return FAILURE;

  htab->size++;
  return SUCCESS;
}

int htab_lookup(htab_t *htab, int key, float *place) {

  int k = hash(key) % htab->cap;
  node_t *node = htab->table[k];

  // linear search through this list entry in the hash table.
  while (node) {
    if (node->key == key) {
      *place = node->val;
      return SUCCESS;
    }
    node = node->next;
  }

  return FAILURE;
}

int htab_delete(htab_t *htab, int key) {
  if (!htab)
    return FAILURE;
  int i = hash(key) % htab->cap;
  return list_delete(htab->table[i], key);
}


void htab_print(htab_t *htab) {
  if (!htab)
    fprintf(stderr, "attempted to print uninitialized htab\n");

  else if (htab->size == 0)
    printf("[]\n");

  else {
    printf("[\n");
    for (int i = 0; i < htab->cap; i++) {

      printf(" i == %d: { ", i);
      node_t *node = htab->table[i];
      while (node) {
        printf("(%d -> %.2f) ", node->key, node->val);
        node = node->next;
      }
      printf("}\n");
    }
    printf("]\n");
  }
}

#if DIAGNOSTICS
void htab_print_diagnostics(htab_t *htab) {
  fprintf(stderr, "Diagnostics not implemented for the chaining variant :(\n");
}
#endif

static inline void __htab_expand(htab_t *htab) {

  size_t new_cap     = htab->cap * 2;
  node_t **new_table = calloc(new_cap, sizeof(node_t*));
  if (!new_table) return;

  // for each entry in table of lists ...
  for (int i = 0; i < htab->cap; i++) {
    node_t *node = htab->table[i];

    // move each node in this list to the new table.
    while (node) {
      node_t *tmp = node->next; // since p->next is manipulated by list_push()

      int k = hash(node->key) % new_cap; // each element is, of course, rehashed.
      list_push(new_table + k, node);

      node = tmp;
    }
  }

  // free only old table, not its sublists, since new table points to these now.
  free(htab->table);

  htab->table = new_table;
  htab->cap   = new_cap;
}


/*
 * LIST STUFF
 */

void list_destroy(node_t *head) {
  node_t *node = head;
  while (node) {
    node_t *tmp = node->next;
    free(node);
    node = tmp;
  }
}


inline int list_push_new(node_t **head, int key, float val) {
  node_t *new = malloc(sizeof(node_t));
  if (!new)
    return FAILURE;
  new->key  = key;
  new->val  = val;
  return list_push(head, new);
}

inline int list_push(node_t **head, node_t *new) {
  if (head) {
    new->next = *head;
    *head = new;
    return SUCCESS;
  }
  return FAILURE;
}

int list_delete(node_t *head, int key) {
  if (!head)
    return FAILURE;

  node_t *node = head;
  node_t *prev = NULL;

  while (node) {
    if (node->key == key) {

      if (prev)
        prev->next = node->next;

      free(node);

      return SUCCESS;
    }
    prev = node;
    node = node->next;
  }
  return FAILURE;
}
