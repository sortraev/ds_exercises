#include "tree.h"


struct node_t {
  int   key;
  float val;

  node_t *p, *l, *r;
};

struct tree_t {
  node_t *root;
  int size;

#if DIAGNOSTICS
  int max_height;
  int num_insert_success;
  int num_insert_failure;
  int num_replacements;
  int num_delete_success;
  int num_delete_failure;
  int num_lookup_success;
  int num_lookup_failure;
#endif
};



static node_t *__node_create(int key, float val);
static node_t *__tree_lookup(tree_t *tree, int key);
static void    __tree_destroy(node_t *t);

static node_t *__node_succ(node_t *t);
static int     __tree_transplant(tree_t *tree, node_t *u, node_t *v);

static int     __check_tree(node_t *t);
static void    __tree_print(node_t *t);


tree_t *tree_create() {
  // no need to NULL/zero-initialize since calloc does that for us.
  // also no need to error check since user is expected to check for NULL.
  return calloc(1, sizeof(tree_t));
}

void tree_destroy(tree_t *tree) {
  if (tree)
    __tree_destroy(tree->root);
  free(tree);
}


int tree_insert(tree_t *tree, int key, float val) {

  if (!tree)
    return FAILURE;

  node_t *t = tree->root;
  node_t *p = NULL;

  while (t) {

    // key already exists; replace existing value.
    if (t->key == key) {
      t->val = val;
#if DIAGNOSTICS
      tree->num_replacements++;
      tree->num_insert_success++;
#endif
      return SUCCESS;
    }

    p = t;
    t = key < t->key ? t->l : t->r;
  }

  // key does not yet exist; insert new node.
  node_t *new = __node_create(key, val);
  if (!new)
    return FAILURE;

  if (p)                 // if new node has a parent ..
    if (key < p->key) p->l = new; // set this parent's child pointer.
    else              p->r = new;
  else tree->root = new; // else, this node is the new root. 

  new->p = p;

  tree->size++;
#if DIAGNOSTICS
  tree->num_insert_success++;
#endif
  return SUCCESS;
}

int tree_delete(tree_t *tree, int key) {

  node_t *d = __tree_lookup(tree, key);

  if (!d) {
#if DIAGNOSTICS
    tree->num_delete_failure++;
#endif
    return FAILURE;
  }

  // if d has only one child, simply swap in the other subtree.
  if (!d->l)
    __tree_transplant(tree, d, d->r);
  else if (!d->r)
    __tree_transplant(tree, d, d->l);

  else {
    // if d has two children, we want to replace d wth its successor.
    node_t *d_succ = __node_succ(d);
    if (d_succ->p != d) { // if d is *not* parent of d_succ, and d_succ has a
                          // right child, replace d_succ with its right
                          // child first.
      __tree_transplant(tree, d_succ, d_succ->r);
      d->r->p   = d_succ;
      d_succ->r = d->r;
    }

    __tree_transplant(tree, d, d_succ);
    d->l->p   = d_succ;
    d_succ->l = d->l;
  }

  free(d);

  tree->size--;

#if DIAGNOSTICS
  tree->num_delete_success++;
#endif
  return SUCCESS;
}

int tree_lookup(tree_t *tree, int key, float *place) {
  if (!tree) return FAILURE;

  node_t *found = __tree_lookup(tree, key);
  if (!found) {
#if DIAGNOSTICS
    tree->num_lookup_failure++;
#endif
    return FAILURE;
  }

  *place = found->val;
#if DIAGNOSTICS
  tree->num_lookup_success++;
#endif
  return SUCCESS;
}

void tree_print(tree_t *tree) {
  if (tree) __tree_print(tree->root);
  else      fprintf(stderr, ">> tree null\n");
}

int tree_verify(tree_t *tree) {
  return tree && __check_tree(tree->root);
}

int node_get_key(node_t *node) {
  return node ? node->key : 0;
}




/* INTERNALS */
node_t *__node_create(int key, float val) {

  node_t *node = calloc(1, sizeof(node_t));
  if (!node) {
    fprintf(stderr, "__node_create(): failed to allocate node.\n");
    return NULL;
  }

  node->key = key;
  node->val = val;

  return node;
}

void __tree_destroy(node_t *t) {
  if (t) {
    __tree_destroy(t->l);
    __tree_destroy(t->r);
    free(t);
  }
}


node_t *__tree_lookup(tree_t *tree, int key) {
  if (!tree)
    return NULL;

  node_t *t = tree->root;
  while (t) {
    if (t->key == key)
      return t;
    t = key < t->key ? t->l : t->r;
  }

  return NULL;
}


int __tree_transplant(tree_t *tree, node_t *u, node_t *v) {
  /*
   * transplant the subtree rooted in v into the current position of the subtree
   * rooted in u. 
   * the node u is not freed and the subtree rooted in u is lost if u's children
   * are not re-adopted after this function returns.
   *
   */

  if (!tree || !u)
    return FAILURE;

  if (tree->root == u)   // if u is the root of the tree
    tree->root = v;
  else if (u == u->p->l) // if u is a left child, v is now a left child.
    u->p->l = v;
  else if (u == u->p->r) // if u is a right child, v is not a right child.
    u->p->r = v;

  // if v is not a leaf node, better set its parent pointer.
  if (v)
    v->p = u->p;

  return SUCCESS;
}


static inline node_t *__node_succ(node_t *t) {
  /*
   * find the successor to the node t.
   */
  if (!t)
    return NULL;
  t = t->r;
  while (t && t->l) t = t->l;
  return t;
}


int __check_tree(node_t *t) {
  /*
   * given a tree t, checks that it satisfies the min tree property.
   * this is satisfied if for every node t in the tree, t->key is *strictly*
   * larger than t->l->key and smaller than or equal to t->r->key OR t is a leaf
   * node.
   *
   */

  return !t || // leaf nodes trivally satisfy property.
         ((!t->l || t->l->key <  t->key) &&          // check left  child, if any.
          (!t->r || t->r->key >= t->key) &&          // check right child, if any.
          __check_tree(t->l) && __check_tree(t->r)); // recursively check subtrees.
}



void __tree_print(node_t *node) {
  if (node) {

    printf("(%3d, %2.2f, %3d, %3d, %3d)\n",
        node->key, node->val, node_get_key(node->p),
                              node_get_key(node->l),
                              node_get_key(node->r));
    __tree_print(node->l);
    __tree_print(node->r);
  }
}


int tree_get_size(tree_t *tree) {
  return tree ? tree->size : -1;
}


#if DIAGNOSTICS
void tree_print_diagnostics(tree_t *tree) {
  if (tree)
    printf("/-----------------------------------\\\n"
           "| is a min tree:         %10s |\n"
           "|                                   |\n"
           "| tree size:             %10d |\n"
           "|                                   |\n"
           "| successful insertions: %10d |\n"
           "| failed     insertions: %10d |\n"
           "| replacements:          %10d |\n"
           "|                                   |\n"
           "| successful deletions:  %10d |\n"
           "| failed     deletions:  %10d |\n"
           "|                                   |\n"
           "| successful lookups:    %10d |\n"
           "| failed     lookups:    %10d |\n"
           "\\-----------------------------------/\n",
           tree_verify(tree) ? "YES" : "NO",
           tree->size,
           tree->num_insert_success, tree->num_insert_failure, tree->num_replacements,
           tree->num_delete_success, tree->num_delete_failure,
           tree->num_lookup_success, tree->num_lookup_failure);
}

#endif
