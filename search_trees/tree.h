#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#define DIAGNOSTICS 1

#define SUCCESS 1
#define FAILURE 0


typedef struct node_t node_t;
typedef struct tree_t tree_t;


/*
 * tree API
 */
tree_t *tree_create();
void tree_destroy(tree_t *tree);

int tree_insert(tree_t *tree, int key, float val);
int tree_delete(tree_t *tree, int key);
int tree_lookup(tree_t *tree, int key, float *place);

int tree_get_size(tree_t *tree);

void tree_print(tree_t *tree);

#if DIAGNOSTICS
int tree_verify(tree_t *tree);
void tree_print_diagnostics(tree_t *tree);
#endif
