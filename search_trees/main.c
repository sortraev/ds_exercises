#include "tree.h"
#include <stdlib.h>
#include <time.h>


int main() {

  srand(time(NULL));

  printf("creating empty tree ...\n");
  tree_t *tree = tree_create();
  tree_print_diagnostics(tree);

  printf("checking tree: %s\n", tree_verify(tree) ? "SUCCESS" : "FAILURE");


  const int num_insertions = 8473128;
  const int num_deletions  = 2187472;

  printf("inserting %d random key/value pairs ...\n", num_insertions);
  for (int i = 0; i < num_insertions; i++) {
    int   key = rand() & 0xffffff;
    float val = ((float) rand() / RAND_MAX) * 100.0f;
    tree_insert(tree, key, val);
  }
  printf(">> after insertion: size == %d\n", tree_get_size(tree));

  printf("deleting %d random keys ...\n", num_deletions);
  for (int i = 0; i < num_deletions; i++) {
    int key = rand() & 0xffffff;
    tree_delete(tree, key);
  }

  tree_print_diagnostics(tree);

  tree_destroy(tree);
}
